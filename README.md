# Command Line Banking Application
---

### The command line application (CLA) is written in Java and has a data persistence solution; MySQL Server. Connecting to the database is accomplished through both, the JDBC driver/MySQL connector from oracle, and the java.SQL libraries. 

### The app has two accounts, and two account types: standard and limited. Each account is imitated a hardcoded balance. Constraints have been placed on the limited account – withdraw/deposit amounts are capped at (50.00). 

### Unit tests have been included to test a couple of methods and expected variable initialisations. The Junit framework was employed here. Conditional statements have been employed to inform user if withdraw/deposit has succeeded/failed; additionally, try, catch statements have been employed to catch any errors and print them to console on behalf of the ‘java.SQL’ libraries. For example, if the user credentials are incorrect, the app cannot connect to the MySQL server; thus, an error messaged is logged to the terminal.

### Below are screen shots detailing the exact SQL queries. Note, a combination of SQL statements and prepared SQL statements are used. Two kinds of statements are needed depending on the query being executed. Read queries do not requires do not require prepared statements as the variable being inserted is not actually updating any records. In contrast, an insert statement needs prepared statements. Reason being, the query will be precompiled once and then at compile the additional values which are denoted as ‘ ? ‘ placeholders will be replaced with java variables. 
---
## There are a total of 3 operations to be executed on behalf of a user:

1. Connect/login to Database
1. View balance available and overdraft remaining – reading from account table in database
1. Withdrawing/deposing the column values are being updated – updating values in account table

## Connecting to database server, with a role possessing only READ and Update privileges to the accounts table.


---

## View balance available and overdraft for specified account id.

---

## Withdrawing funds from account for specified account id.

---

## Depositing funds into account for specified account id.

---

